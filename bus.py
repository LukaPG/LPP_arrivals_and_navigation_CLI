import json
from urllib import request, parse
from datetime import datetime, timedelta
import sys

api = "http://data.lpp.si/"


def readData(url):
    """Returns dict based on json data in the url."""
    url = api+url
    try:
        return json.load(request.urlopen(url))
    except:
        return json.loads("{}")


def isWithin2Hours(time, later):
    now = datetime.now().time()
    laterer = later + timedelta(hours=2)
    return later.time() <= time.time() <= laterer.time()


def getArrivalsTo(stationID, routeID):
    """Yield datetime objects with arrival times."""
    json_data = readData("timetables/getArrivalsOnStation?route_int_id={}&station_int_id={}".format(routeID, stationID))
    try:
        for arrival in json_data["data"]:
            time = arrival["arrival_time"]
            dt = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%fZ")
            yield dt
    except:
        return False


def getStationID(stationName):
    """Yields station int_ids that correspond to a station name."""
    stations = open("stations.json", encoding="utf-8")
    data = json.load(stations)
    data = data["data"]
    for station in data:
        if station["name"].lower() == stationName:
            yield station["int_id"]


def getBusID(busNumber):
    """Returns set of all bus_ids that correspond to a bus number."""
    json_data = readData("routes/getRoutes?route_name={}".format(busNumber))
    data = json_data["data"]
    routes = set()
    for route in data:
        routes.add(route["int_id"])
        if route["opposite_route_int_id"] is not None:
            routes.add(route["opposite_route_int_id"])
    return routes


def getBusNumber(busID):
    """Returns the bus number and its direction corresponding to its int_id"""
    json_data = readData("routes/getRoutes")
    data = json_data["data"]
    for route in data:
        if route["int_id"] == busID:
            busNumber = route["group_name"]
            direction = route["route_name"].split(";")[0]
            return busNumber, direction


def getRoutesOnStation(stationID):
    """Yields all bus_ids that stop on station."""
    url = "stations/getRoutesOnStation?station_int_id={}".format(stationID)
    json_data = readData(url)
    routes = json_data["data"]
    for route in routes:
        yield route["int_id"]


def getLiveArrivalsTo(stationID):
    """Yield tuples of (busNum, direction, eta)"""
    json_data = readData("timetables/liveBusArrival?station_int_id={}".format(stationID))
    arrivals = json_data["data"]  # array of dicts
    for arrival in arrivals:
        yield (arrival["route_number"], arrival["route_name"].strip(), arrival["eta"])


def findAllBussesArrivingToStationAfter(time, stationName, busNum="all"):
    stationID = getStationID(stationName)  # this is a generator
    arrivals = {}  # dict of bus_id:(generator of arrival times)
    for int_id in stationID:
        for routeID in getRoutesOnStation(int_id):
            a = getArrivalsTo(int_id, routeID)
            if a:
                arrivals[routeID] = getArrivalsTo(int_id, routeID)
        for busID in arrivals:
            try:
                if busNum == "all" or busID in getBusID(busNum):
                    toPrint = ""
                    busNumber, direction = getBusNumber(busID)
                    toPrint += "The bus number {} in the direction of {} will arrive at ".format(busNumber, direction)
                    for timeZulu in arrivals[busID]:
                        timeCET = timeZulu
                        strTime = timeCET.strftime('%H:%M')
                        if isWithin2Hours(timeCET, time):
                            toPrint += strTime + ", "
                    if toPrint[-2:] != "t ":
                        print(toPrint[:-2] + ".")
            except:
                continue


def findLiveArrivalsToStation(stationName):
    stationID = getStationID(stationName)
    arrivals = {}  # dict: {busNumber:[(dir, ETA),...]}
    for int_id in stationID:
        for arrival in getLiveArrivalsTo(int_id):
            if arrival[0] in arrivals:
                arrivals[arrival[0]].append((arrival[1], arrival[2]))
            else:
                arrivals[arrival[0]] = [(arrival[1], arrival[2])]

    for busNum in arrivals:
        for arr in arrivals[busNum]:
            toPrint = ""
            direction, ETA = arr
            toPrint += "The bus number {} in the direction of {} will arrive to {} in {} minutes.".format(busNum, direction, stationName, ETA)
            print(toPrint)

    if not arrivals:
        print("No busses are arriving in the near future.")


def isTime(string):
    try:
        datetime.strptime(string, '%H:%M')
        return True
    except:
        return False


def isInt(string):
    try:
        string = int(string)
        return True
    except:
        return False


def addStationNameToHistory(stationName):
    with open("history", 'a') as history:
        history.write(parse.quote(stationName)+"\n")


args = sys.argv[1:]
# bus [number] [actual] arrivals [after time] to "station name"


if args[0] == "arrivals" and args[1] == "to":
    stationName = " ".join(args[2:]).lower()
    findAllBussesArrivingToStationAfter(datetime.now(), stationName)
    addStationNameToHistory(stationName)
elif args[0] == "arrivals" and args[1] == "after" and isTime(args[2]) and args[3] == "to":
    time = datetime.strptime(args[2], '%H:%M')
    stationName = " ".join(args[4:]).lower()
    findAllBussesArrivingToStationAfter(time, stationName)
    addStationNameToHistory(stationName)
elif isInt(args[0]) and args[1] == "arrivals" and args[2] == "to":
    stationName = " ".join(args[3:]).lower()
    findAllBussesArrivingToStationAfter(datetime.now(), stationName, busNum=args[0])
    addStationNameToHistory(stationName)
elif isInt(args[0]) and args[1] == "arrivals" and args[2] == "after" and isTime(args[3]) and args[4] == "to":
    time = datetime.strptime(args[3], '%H:%M')
    stationName = " ".join(args[5:]).lower()
    findAllBussesArrivingToStationAfter(time, stationName, busNum=args[0])
    addStationNameToHistory(stationName)
# live arrivals
elif args[0] == "actual" and args[1] == "arrivals" and args[2] == "to":
    stationName = " ".join(args[3:]).lower()
    findLiveArrivalsToStation(stationName)
    addStationNameToHistory(stationName)
else:
    print("Usage: \nbus [number] arrivals [after HH:MM] to 'Station Name' \nor\nbus actual arrivals to 'Station Name'\n(without quotes)")


