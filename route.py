import json, sys, re
from urllib import request, parse
google = "https://maps.googleapis.com/maps/api/directions/json?"


#route to place
def readData(url):
    """Returns dict based on json data in the url."""
    url = google+url
    #try:
    return json.load(request.urlopen(url))
    #except:
        #return json.loads("{}")


def getRouteData(stationName):
    last_station = ""
    key = ""
    with open("history", 'r') as hist:
        lines = hist.read().splitlines()
        last_station = lines[-1].split(" ")
        last_station = " ".join(last_station)
        if last_station == "NOSTATION":
            print("You need to select a station by using the command 'bus' first.")
            return
    with open("google_api.key", 'r') as api_key:
        key = api_key.readline()

    url = "origin={}&destination={}&region=si&mode=transit&key={}".format(last_station, parse.quote(stationName), key)
    response = readData(url)
    data = response["routes"][0]["legs"][0]  # dict containing dep_time, arr_time, duration and steps
    dep_time = data["departure_time"]["value"]  # s since epoch
    arr_time = data["arrival_time"]["value"]
    duration = data["duration"]["text"].split(" ")[0]  # number of minutes
    steps    = data["steps"]  # array of dicts

    return dep_time, arr_time, duration, steps


args = sys.argv[1:]


if args[0] == "to" and len(args) > 1:
    try:
        stationName = " ".join(args[1:]).lower()
        dT, aT, dur, steps = getRouteData(stationName)
        with open("times", 'a') as t:
            t.write("{} {} {}\n".format(dT, aT, dur))
        for step in steps:
            duration = step["duration"]["text"].split(" ")[0]
            if step["travel_mode"] == "TRANSIT":
                arr_stop = step["transit_details"]["arrival_stop"]["name"]
                direction= step["transit_details"]["headsign"].split(" - ")[-1]
                try:
                    bus_num = step["transit_details"]["line"]["short_name"]
                    print("Take the bus number {} towards {} for {} minutes. Get off at {}.".format(bus_num, direction, duration, arr_stop))
                except:
                    print("Take the bus towards {} for {} minutes. Get off at {}.".format(direction, duration, arr_stop))
            elif step["travel_mode"] == "WALKING":
                print("To " + step["html_instructions"][0].lower() + step["html_instructions"][1:] + ":")
                for part in step["steps"]:
                    string = part["html_instructions"]
                    string = re.sub('<[^<]+?>', '', string)
                    print(string)
    except:
        print("Invalid station.")
else:
    print("Usage:\nroute to 'station name' (without quotes)")

