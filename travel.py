from sys import argv
from datetime import datetime

with open("times", 'r') as t:
    data = t.read().splitlines()[-1]
    departure, arrival, duration = data.split(" ")
departure = datetime.fromtimestamp(int(departure))
dep = departure.strftime("%H:%M")

arrival = datetime.fromtimestamp(int(arrival))
arr = arrival.strftime("%H:%M")


args = " ".join(argv[1:])
if args == "time":
    print("Your travel will take {} minutes.".format(duration))
elif args == "data":
    print("Your route will start at {}, end at {} and take {} minutes.".format(dep, arr, duration))
else:
    print("Usage:\ntravel time or\ntravel data")